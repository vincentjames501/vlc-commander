var db;

////////// Build List and Showing Currently Online/Offline Computers Functions //////////

function populateExistingSavedComputers(tx) {
    $.mobile.showPageLoadingMsg();
    tx.executeSql('SELECT * FROM HOST', [], querySuccess, errorFetchingSavedComputer);
}

function querySuccess(tx, results) {
    var len = results.rows.length;
    $("#savedComputers").html("");
    $("#savedComputers").append($("<li>").attr("data-role", "list-divider").text("Saved Computers"));
    for (var i = 0; i < len; i++) {
        var statusId = "#status_" + results.rows.item(i).id;
        testIPSync(results.rows.item(i).ip, results.rows.item(i).port, statusId);
        status = "Scanning...";

        var link = $("<a>").attr("onClick", "loadSavedComp('" + results.rows.item(i).id + "','" + results.rows.item(i).ip + "', '" + results.rows.item(i).port + "', '" + results.rows.item(i).name + "');").html(results.rows.item(i).name + "<span id='status_" + results.rows.item(i).id + "' class='ui-li-count'>" + status + "</span>");

        $("#savedComputers").append($("<li>").append(link));
    }
    $("#savedComputers").listview("refresh");
    $.mobile.hidePageLoadingMsg();
}

function errorFetchingSavedComputer(err) {
    navigator.notification.alert("There was an error fetcing saved computers!");
    $.mobile.hidePageLoadingMsg();
}

////////// Build Initial Database Functions //////////

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    db = window.openDatabase("VLCCommander", "1.0", "VLCCommander", 1000000);
    db.transaction(populateDB, errorCreatingTable, function(){});
}

function populateDB(tx) {
    //tx.executeSql('DROP TABLE IF EXISTS HOST');
    tx.executeSql('CREATE TABLE IF NOT EXISTS HOST (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ip unique, port, name unique)');
    populateExistingSavedComputers(tx);
}

function errorCreatingTable(err) {
    navigator.notification.alert("There was an error creating the database tables!");
    $.mobile.hidePageLoadingMsg();
}

////////// Adding Computer Functions //////////

$(document).delegate('#addComputerButton', 'click', function () {
    $(this).simpledialog2({
        mode: 'blank',
        headerText: 'Add Computer',
        headerClose: true,
        fullScreen: true,
        fullScreenForce: true,
        blankContent : "<center><h3>Name</h3><input id='addComputerName' type='text' value=''/><h3>IP Address</h3><input id='addIP' type='text' value=''/><h3>Port</h3><input id='addPort' type='text' value='8080'/><a data-icon='plus' data-role='button' href='index.html' data-theme='c' onClick='addComputer();'>Add</a></center>"
    });
});

function addComputer() {
    var name = $("#addComputerName").attr("value");
    var ip = $("#addIP").attr("value");
    var port = $("#addPort").attr("value");
    close();
    var addComputerTransaction = function (tx) {
        tx.executeSql('INSERT INTO HOST (ip, port, name) VALUES ("' + ip + '","' + port + '" ,"' + name + '")');
        populateExistingSavedComputers(tx);
    };
    db.transaction(addComputerTransaction, errorAddingComputerCB, function(){});
}

function errorAddingComputerCB(err) {
    navigator.notification.alert("There was an error saving the computer.  Does it already exist?");
    $.mobile.hidePageLoadingMsg();
}

////////// Modifying Computer Functions //////////

function modifyEntry(id, address, port, name) {
    $(this).simpledialog2({
        mode: 'blank',
        headerText: 'Modify Computer',
        headerClose: true,
        fullScreen: true,
        fullScreenForce: true,
        blankContent : "<center><input id='modifyingId' type='hidden' value='" + id + "'/><h3>Name</h3><input id='modifiedComputerName' type='text' value='" + name + "'/><h3>IP Address</h3><input id='modifiedIP' type='text' value='" + address + "'/><h3>Port</h3><input id='modifiedPort' type='text' value='" + port + "'/><a data-icon='gear' data-role='button' href='index.html' data-theme='c' onClick='modifyComputer()'>Modify</a>"
    });
}

function modifyComputer() {
    var id = $("#modifyingId").attr("value");
    var name = $("#modifiedComputerName").attr("value");
    var ip = $("#modifiedIP").attr("value");
    var port = $("#modifiedPort").attr("value");
    close();
    var modifyComputerTransaction = function (tx) {
        tx.executeSql('UPDATE HOST SET ip="' + ip + '", port="' + port + '", name="' + name + '" where id="' + id + '"');
        populateExistingSavedComputers(tx);
    };
    db.transaction(modifyComputerTransaction, errorModifyingComputerCB, function(){});
}

function errorModifyingComputerCB(err) {
    navigator.notification.alert("There was an error modifying the computer!");
    $.mobile.hidePageLoadingMsg();
}


////////// Deleting Computer Functions //////////

function deleteEntry(id) {
    close();
    var deleteEntryTransaction = function (tx) {
        tx.executeSql('DELETE FROM HOST WHERE ID = "' + id + '"');
        populateExistingSavedComputers(tx);
    };
    db.transaction(deleteEntryTransaction, errorDeletingComputerCB, function () {
    });
}

function errorDeletingComputerCB(err) {
    navigator.notification.alert("There was an error removing the computer!");
    $.mobile.hidePageLoadingMsg();
}

////////// Loading and Saving Found Computer Functions //////////

function loadFoundComp(address, port) {
    loadComp(address, port);
    var link = $("<a>").attr('data-role', 'button').attr('href', 'index.html').attr('data-icon', 'plus').attr('data-theme', 'c').attr('onClick', "saveFoundComputer('" + address + "','" + port + "');").text("Save Computer");
    var div = $("<div>").append(link);
    $("#saveComputerName").remove();
    $("<div>").simpledialog2({
        mode: 'blank',
        headerText: 'Action',
        headerClose: true,
        fullScreen: true,
        fullScreenForce: true,
        blankContent : "<input id='saveComputerName' type='text'/>" + div.html() + "<a data-role='button'  data-transition='slide'  data-theme='c' data-icon='check' href='player.html'>Just Go</a>"
    });
}

function saveFoundComputer(address, p) {
    name = $("#saveComputerName").attr("value");
    ip = address;
    port = p;
    var saveFoundComputerTransaction = function (tx) {
        tx.executeSql('INSERT INTO HOST (ip, port, name) VALUES ("' + ip + '","' + port + '" ,"' + name + '")');
        populateExistingSavedComputers(tx);
    };
    db.transaction(saveFoundComputerTransaction, errorAddingComputerCB, function () {
    });
}

////////// Launching VLC Commander //////////

function loadComp(address, port) {
    serverAddress = address;
    serverPort = port;
}

var serverAddress = "";
var serverPort = "";

function loadSavedComp(id, address, port, name) {
    var fullHTML;
    if ($("#status_" + id).text() == "Online") {
        fullHTML = "<center><a data-theme='c' data-icon='check' data-role='button' href='player.html'>Load Commander</a><a data-role='button' data-theme='c' data-icon='delete'  data-transition='slide'  onClick='deleteEntry(\"" + id + "\");' href='index.html'>Delete</a><a data-role='button' data-icon='gear' data-theme='c' onClick='modifyEntry(\"" + id + "\", \"" + address + "\", \"" + port + "\" , \"" + name + "\" );'>Modify</a>";
    } else {
        fullHTML = "<center><a data-role='button' data-theme='c' data-icon='delete'  onClick='deleteEntry(\"" + id + "\");' href='index.html'>Delete</a><a data-role='button' data-icon='gear' data-theme='c' onClick='modifyEntry(\"" + id + "\", \"" + address + "\", \"" + port + "\" , \"" + name + "\" );'>Modify</a>"
    }
    $("<div>").simpledialog2({
        mode: 'blank',
        headerClose: true,
        headerText: 'Action',
        fullScreen: true,
        fullScreenForce: true,      
        blankContent : fullHTML
    });

    loadComp(address, port);
}

////////// Simple Dialog Helpers //////////

function close() {
    $(document).trigger('simpledialog', {'method':'close'});
}