function scanForComputers() {
    $.mobile.showPageLoadingMsg();
    $("#foundComputers").html("");
    $("#foundComputers").append($("<li>").attr("data-role", "list-divider").text("Found Computers"));
    $("#foundComputers").listview("refresh");

    setTimeout('scanIPs("192.168.0.")', 0);
    setTimeout('scanIPs("192.168.1.")', 1000);
    setTimeout('scanIPs("192.168.2.")', 2000);
    setTimeout('scanIPs("192.168.10.")', 3000);
    setTimeout('scanIPs("10.10.1.")', 4000);
    setTimeout('scanIPs("10.10.0.")', 5000);

    setTimeout("hideLoadingMessage()", 8000);
}

function hideLoadingMessage() {
    $.mobile.hidePageLoadingMsg();
}

function scanIPs(baseURI) {
    for (var i = 0; i < 256; i++) {
        testIP("http://" + baseURI + i + ":8080/requests/status.json", "" + baseURI + i);
    }
}

function testIP(url, server) {
    $.ajax({
        url:url,
        async:true,
        timeout:1000,
        success:function (data) {
            var link = $("<a>").text(server);
            link.click(function (e) {
                loadFoundComp(server, '8080');
            });
            $("#foundComputers").append($("<li>").append(link));
            $("#foundComputers").listview("refresh");
        }
    });
}
function checkOlderVersion(baseURI, port, statusId){
    $.ajax({
        url:"http://" + baseURI + ":" + port + "/requests/status.xml",
        async:false,
        timeout:1000,
        success:function (data) {
            $(statusId).text("Upgrade");
        },
        error: function() {
            $(statusId).text("Offline");
        }
    });
}

function testIPSync(baseURI, port, statusId) {
    $.ajax({
        url:"http://" + baseURI + ":" + port + "/requests/status.json",
        async:true,
        timeout:1000,
        success:function (data) {
            $(statusId).text("Online");
        },
        error: function() {
            checkOlderVersion(baseURI, port, statusId);
        }
    });

}