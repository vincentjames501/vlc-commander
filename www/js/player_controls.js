var refreshing = false;
var currentPlayList = {};
var currentlyPlayingTitle = "";
var playingState;

function serverReady() {
    $("#play_bar").hide();
    $("#play").click(play);
    $("#stop").click(stop);
    $("#next").click(next);
    $("#previous").click(previous);
    $("#full_screen").click(fullScreen);
    $("#play_bar").change(playBarChanged);
    $("#volume_bar").change(volumeBarChanged);
    $(".ui-slider-input").hide();
    updateStatus();
}

$(document).ready(function () {
    $("#refreshButton").click(refresh);
    $('.browsePage').live('pageshow', browsePageInit);
    $('.playlistPage').live('pageshow', playListPageInit);
    $('.streamInfoPage').live('pageshow', streamInfoPageInit);
    $('.playerPage').live('pageshow', playerPageInit);
});

function refresh() {
    db.transaction(populateDB, errorFetchingSavedComputer, function(){});
}

function browsePageInit() {
    changeDirectory("~", "directory");
}

function playListPageInit() {
    currentPlayList = {};
}

function streamInfoPageInit() {
    currentlyPlayingTitle = "";
}

function playerPageInit() {
    currentlyPlayingTitle = "";
    currentPlayList = {};
    serverReady();
}

function playFileFromPlayList() {
    if ($("#playListFields .ui-radio-on").length == 0) {
        return;
    }
    var playListIndex = $("input[name='radio-choice-1']:checked").attr("id");
    var currentPlayListItem = currentPlayList[playListIndex];
    playFileInPlayList(currentPlayListItem.id);
}

function deleteFileFromPlayList() {
    if ($("#playListFields .ui-radio-on").length == 0) {
        return;
    }
    var playListIndex = $("input[name='radio-choice-1']:checked").attr("id");
    var currentPlayListItem = currentPlayList[playListIndex];
    deleteFileInPlayList(currentPlayListItem.id);
}

function updateStatus() {
    var status = getStatus();
    updatePlayBarStatus(status);
    $("#play_bar").attr("max", status.length);
    $("#play_bar").attr("value", status.time);
    $("#volume_bar").attr("value", status.volume);
    refreshing = true;
    $("#play_bar").slider('refresh');
    $("#volume_bar").slider('refresh');
    if (playingState != status.state) {
        playingState = status.state;
        if (playingState == "playing") {
            $("#play").attr("src", "icons/pause.png");
        } else {
            $("#play").attr("src", "icons/play.png");
        }
    }
    updateChecksForPlaylistControls(status);
    updatePlaylist();
    updateInfo(status);
    setTimeout("updateStatus()", 750);
}

function updateChecksForPlaylistControls(status) {
    updateCheckForPlayListControl(status.random, "shuffleListItem");
    updateCheckForPlayListControl(status.loop, "loopListItem");
    updateCheckForPlayListControl(status.repeat, "repeatListItem");
}

function updateCheckForPlayListControl(status, id) {
    if (status == true && $("#" + id)) {
        $("#" + id + " span").removeClass("ui-icon-delete").addClass("ui-icon-check");
    } else {
        $("#" + id + " span.ui-icon").removeClass("ui-icon-check").addClass("ui-icon-delete");
    }
}

function updateInfo(status) {
    var information;
    if (status.hasOwnProperty("information")) {
        information = status.information;
    } else {
        $("#info").html("<h3>No Currently Playing Stream!</h3>");
        return;
    }
    if (currentlyPlayingTitle == information.category.meta.title) {
        return;
    }

    currentlyPlayingTitle = information.category.meta.title;

    var mainInfoDiv = $("<div>");
    var currentlyPlayingDiv = $('<div>').attr("data-role", "collapsible").attr("data-content-theme", "c");
    currentlyPlayingDiv.append($('<h3>').text("Currently Playing"));
    currentlyPlayingDiv.append($('<p>').text(information.category.meta.filename));
    mainInfoDiv.append(currentlyPlayingDiv);
    for (var infoIndex in information.category) {
        if (infoIndex != "meta") {
            var infoElement = information.category[infoIndex];
            var nameOfStreamDiv = $('<div>').attr("data-role", "collapsible").attr("data-content-theme", "c");
            nameOfStreamDiv.append($('<h3>').text(infoIndex));

            for (var infoValue in infoElement) {
                var infoValueObject = infoElement[infoValue];
                var streamInfoDiv = $('<div>').attr("data-role", "collapsible").attr("data-content-theme", "c");
                streamInfoDiv.append($('<h3>').text(infoValue));
                streamInfoDiv.append($('<p>').text(infoValueObject));
                nameOfStreamDiv.append(streamInfoDiv);
            }
            mainInfoDiv.append(nameOfStreamDiv);
        }
    }
    $("#info").html(mainInfoDiv);
    $("#info").trigger('create')
}

function updatePlaylist() {
    var playList = getPlaylist();
    if (playListsAreEqual(playList)) {
        return;
    }
    currentPlayList = playList;
    $("#playListFields").html("");
    for (i in currentPlayList) {
        var currentPlayListItem = currentPlayList[i];
        $("#playListFields").append("<input type=\"radio\" name=\"radio-choice-1\" id=\"" + i + "\" value=\"choice-1\" ></input>");
        $("#playListFields").append("<label for=\"" + i + "\">" + currentPlayListItem.name + "</label>");
    }
    $("#playListFields").fieldcontain();
    $('#playListFields input').checkboxradio();
}

function playListsAreEqual(playList) {
    if (playList.length != currentPlayList.length) {
        return false;
    }
    for (i in playList) {
        var playListItem = playList[i];
        var currentPlayListItem = currentPlayList[i];
        if (playListItem.name != currentPlayListItem.name) {
            return false;
        }
    }
    return true;
}

function updatePlayBarStatus(status) {
    var minutesCurrent = Math.floor((parseFloat(status.time) / 60));
    var secondsCurrent = parseFloat(status.time) % 60;
    var minutesTotal = Math.floor((parseFloat(status.length) / 60));
    var secondsTotal = parseFloat(status.length) % 60;
    minutesCurrent = formatNumber(minutesCurrent);
    secondsCurrent = formatNumber(secondsCurrent);
    minutesTotal = formatNumber(minutesTotal);
    secondsTotal = formatNumber(secondsTotal);
    $("#play_bar_status").html("" + minutesCurrent + ":" + secondsCurrent + " / " + minutesTotal + ":" + secondsTotal);
}

function formatNumber(number) {
    if (number < 10) {
        return "0" + number.toString();
    }
    return number;
}

function buildList(list) {
    $("#file_list").html("");
    for (i in list) {
        var item = list[i];
        item.path = item.path.replace(/\\/g, "/");
        ;
        var method = "changeDirectory(\"" + item.path + "\", \"" + item.type + "\")";
        $("#file_list").append("<li><a href='#' onClick='" + method + "'>" + item.name + "</a></li>");
    }
    $("#file_list").listview("refresh");
}

function changeDirectory(directory, type) {
    if (type == "file") {
        if ($("input[type='checkbox']").attr("checked")) {
            enqueue(directory);
        } else {
            playFile(directory);
        }
    } else {
        var list = getList(directory);
        buildList(list);
    }
}
