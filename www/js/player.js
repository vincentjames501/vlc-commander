var errorCount = 0;
function getStatus() {
    var status = new Array();
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json",
        async:false,
        success:function (data) {
            status = JSON.parse(data);
            status.volume = status.volume / 512.0 * 2.0 * 100.0;
        },
        error:function () {
            errorCount++;
            if (errorCount == 5) {
                errorCount = 0;
                navigator.notification.alert('Could not connect to VLC!');
                window.location = 'index.html';
            }
        }
    });
    return status;
}

function seek(percent) {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=seek&val=" + percent + "%"
    });
}

function repeat() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_repeat"
    });
}

function loop() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_loop"
    });
}

function shuffle() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_random"
    });
}

function empty() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_empty"
    });
}

function stop() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_stop"
    });
}

function fullScreen() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=fullscreen"
    });
}

function play() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_pause"
    });
}

function next() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_next"
    });
}

function previous() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_previous"
    });
}

function key(key) {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=" + key
    });
}

function audioDelayUp() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=audiodelay-up"
    });
}

function audioDelayDown() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=audiodelay-down"
    });
}

function subtitleDelayUp() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=subdelay-up"
    });
}

function subtitleDelayDown() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=subdelay-down"
    });
}

function faster() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=faster"
    });
}

function cropAspectRatio() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=crop"
    });
}

function slower() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=slower"
    });
}

function aspectRatio() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=aspect-ratio"
    });
}

function playFile(file) {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=in_play&input=file:///" + escape(file)
    });
}


function enqueue(file) {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=in_enqueue&input=file:///" + escape(file)
    });
}

function playFileInPlayList(id) {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_play&id=" + id
    });
}

function deleteFileInPlayList(id) {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=pl_delete&id=" + id
    });
}

function subtitles() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=subtitle-track"
    });
}

function audioTrack() {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=key&val=audio-track"
    });
}

function setVolume(volume_percent) {
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/status.json?command=volume&val=" + volume_percent
    });
}

function getList(directory) {
    $.mobile.showPageLoadingMsg();
    var list = new Array();
    list[0] = {path:"", name:"All Drives (Windows Only)", type:"directory"};
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/browse.json?dir=" + directory,
        async:false,
        success:function (data) {
            data = JSON.parse(data);
            for (var i in data.element) {
                list[parseInt(i) + 1] = data.element[i];
            }
        }
    });
    $.mobile.hidePageLoadingMsg();
    return list;
}


function getPlaylist() {
    var list = new Array();
    $.ajax({
        url:"http://" + serverAddress + ":" + serverPort + "/requests/playlist.xml",
        async:false,
        success:function (data) {
            for (var i = 1; i < data.childNodes[0].childNodes[1].childNodes.length; i += 2) {
                list[i] = {name:data.childNodes[0].childNodes[1].childNodes[i].attributes.name.nodeValue,
                    id:data.childNodes[0].childNodes[1].childNodes[i].attributes.id.nodeValue,
                    uri:data.childNodes[0].childNodes[1].childNodes[i].attributes.uri.nodeValue};
            }
        }
    });
    return list;
}

function playBarChanged() {
    if (refreshing) {
        refreshing = false;
        return;
    }
    var max = $("#play_bar").attr("max");
    var current = $("#play_bar").attr("value");
    seek((current / max * 100).toFixed(0));
}

function volumeBarChanged() {
    var volume_percent = $("#volume_bar").attr("value");
    volume_percent = Math.floor((parseFloat(volume_percent) * 2.56));
    setVolume(volume_percent);
}